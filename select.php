<?php
if (empty($_GET['page'])) {
    include "awal.php";
} else {
    switch ($_GET['page']) {
        
        //pelanggan
        case ('login'):
            include('pages/login/masuk.php');
            break;
		case ('detailpost'):
            include('detail.php');
            break;
        case ('detailcat'):
            include('detailkategori.php');
            break;
        case ('hasil'):
                include('partikel/hasil_cari.php');
                break;
        case ('proses_komentar'):
            include('partikel/proses.php');
            break;
		case ('proses_saran'):
            include('partikel/prosessaran.php');
            break;
		case ('suggestion'):
            include('saran.php');
            break;
		case ('masukan'):
            include('saran.php');
            break;
        default:
            include('awal.php');
    }
}
?>