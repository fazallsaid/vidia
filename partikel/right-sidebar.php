<div class="col-xl-4 sidebar ftco-animate bg-light pt-5">
	            <div class="sidebar-box pt-md-4">
	              <form action="?page=hasil" method="POST" class="search-form">
	                <div class="form-group">
	                  <span class="icon icon-search"></span>
	                  <input id="search_query" name="judul_story" type="text" class="form-control" placeholder="Cari Cerpen">
	                </div>
	              </form>
	            </div>
	            <div class="sidebar-box ftco-animate">
	            	<h3 class="sidebar-heading">Kategori</h3>
	              <ul class="categories">
				  <?php
						$b = mysqli_query($connection, "SELECT * FROM kategori ORDER BY id_kategori DESC");
						while ($ass = mysqli_fetch_array($b)):
						$a = mysqli_query($connection, "SELECT * FROM story WHERE id_kategori='$ass[id_kategori]' AND status='P'");
						$c = mysqli_num_rows($a);
						?>
	                <li><a href="?page=detailcat&id_kategori=<?php echo $ass['id_kategori']; ?>"><?php echo $ass['nama_kategori']; ?> <span>(<?php echo $c; ?>)</span></a></li>
					<?php endwhile; ?>
	              </ul>
	            </div>

	            <div class="sidebar-box ftco-animate">
	              <h3 class="sidebar-heading">Cerpen Populer</h3>
				  <?php
				  $b = mysqli_query($connection, "SELECT * FROM story WHERE dibaca > 30 AND status='P' ORDER BY id_story DESC LIMIT 3");
						while ($bss = mysqli_fetch_array($b)): 
				  ?>
	              <div class="block-21 mb-4 d-flex">
	                <a class="blog-img mr-4" style="background-image: url(images/thumbnaila.jpg);"></a>
	                <div class="text">
	                  <h3 class="heading"><a href="#"><?php echo $bss['judul_story']; ?></a></h3>
	                  <div class="meta">
	                    <div><a href="#"><span class="icon-calendar"></span> <?php echo tanggal_indo($bss['tgl_post']); ?></a></div>
	                    <div><a href="#"><span class="icon-person"></span> Vidia Ardiyanti</a></div>
	                    <div><a href="#"><span class="icon-eye"></span> <?php echo $bss['dibaca']; ?>x</a></div>
	                  </div>
	                </div>
	              </div>
				  <?php endwhile; ?>
				  Lihat Semua
	            </div>
	            <div class="sidebar-box ftco-animate">
	              <h3 class="sidebar-heading">Tentangku</h3>
	              <p>
				  <?php echo $tentangku; ?>
				  </p>
	            </div>
	          </div>