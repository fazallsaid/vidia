<div class="comment-form-wrap pt-5">
		                <h3 class="mb-5">Balas <?php echo $komen['nama']; ?></h3>
		                <form action="?page=proses_komentar" method="post" class="p-3 p-md-5 bg-light">
						<input type="hidden" name="id_story" value="<?php echo $story; ?>" />
		                  <div class="form-group">
		                    <label for="name">Nama kamu*</label>
		                    <input type="text" class="form-control" id="name" name="nama">
		                  </div>
		                  <div class="form-group">
		                    <label for="message">Komentar</label>
		                    <textarea name="komentarnya" id="komentar" cols="30" rows="10" class="form-control"></textarea>
		                  </div>
						  <input type="hidden" name="tgl_komen" value="<?php echo date('Y-m-d'); ?>" />
		                  <div class="form-group">
		                    <input type="submit" value="Komentarin" class="btn py-3 px-4 btn-primary">
		                  </div>

		                </form>
		              </div>