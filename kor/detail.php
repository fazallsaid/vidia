<?php
$story = $_GET['id_story'];
$ab = mysqli_query($connection, "SELECT story.*, kategori.* FROM story JOIN kategori ON kategori.id_kategori=story.id_kategori WHERE id_story='$story'");
$baca = mysqli_query($connection, "UPDATE story SET dibaca = dibaca + 1 WHERE id_story='$story'");
$abb = mysqli_fetch_array($ab);
?>
<div class="col-lg-8 px-md-5 py-5">
	    				<div class="row pt-md-4">
	    					<h1 class="mb-3"><?php echo $abb['judul_story']; ?></h1>
		            		<?php echo $abb['isi_story']; ?><br/>
							<p><i>이 논문은 <?php echo $abb['dibaca']; ?>x 읽기되었습니다.</i></p>
		            		<div class="about-author d-flex p-4 bg-light">
								<div class="bio mr-5">
									<img src="../images/thumbnaila1.jpg" alt="Image placeholder" width="100" height="120">
								</div>
								<div class="desc">
								<h3>Vidia Ardiyanti</h3>
								<p><?php echo $tentangku; ?></p>
		              			</div>
		            	</div>


		            <div class="pt-5 mt-5">
					<?php
					$abcc = mysqli_query($connection, "SELECT COUNT(komentarnya) AS jum_komen FROM komentar WHERE id_story='$story'");
					$abcd = mysqli_fetch_array($abcc);
					if ($abcd['jum_komen'] == 0){
						$aaa = "코멘트가 없습니다.";
					}else{
						$aaa = $abcd['jum_komen'] . " 댓글";
					}
					?>
		              <h3 class="mb-5 font-weight-bold"><?php echo $aaa; ?></h3>
		              <ul class="comment-list">
					  <?php
					  $kom = mysqli_query($connection, "SELECT * FROM komentar WHERE id_story='$story'");
					  while ($komen = mysqli_fetch_array($kom)):
						if ($komen > 0){
							$komm = require 'komentar.php';
						}else{
							$komm = "";
						}
					  endwhile; ?>
		              </ul>
		              <!-- END comment-list -->
		              
		              <div class="comment-form-wrap pt-5">
		                <h3 class="mb-5">어떻게 생각하세요? 예 아래에 의견을 보내 주시기 바랍니다!</h3>
		                <form action="?page=proses_komentar" method="post" class="p-3 p-md-5 bg-light">
						<input type="hidden" name="id_story" value="<?php echo $story; ?>" />
		                  <div class="form-group">
		                    <label for="name">사용자 이름*</label>
		                    <input type="text" class="form-control" id="name" name="nama">
		                  </div>
		                  <div class="form-group">
		                    <label for="message">코멘트</label>
		                    <textarea name="komentarnya" id="komentar" cols="30" rows="10" class="form-control"></textarea>
		                  </div>
						  <input type="hidden" name="tgl_komen" value="<?php echo date('Y-m-d'); ?>" />
						  <input type="hidden" name="baca" value="N" />
		                  <div class="form-group">
		                    <input type="submit" value="해설자" class="btn py-3 px-4 btn-primary">
		                  </div>

		                </form>
		              </div>
		            </div>
			    		</div><!-- END-->
			    	</div>