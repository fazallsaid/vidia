<?php
function tanggal_indo($tanggal, $cetak_hari = true)
{
	$hari = array ( 1 =>    '월요일',
				'화요일',
				'수요일',
				'목요일',
				'금요일',
				'토요일',
				'주'
			);
			
	$bulan = array (1 =>   '1월',
				'2월',
				'3월',
				'4월',
				'5월',
				'6월',
				'7월',
				'8월',
				'9월',
				'10월',
				'11월',
				'12월'
			);
	$split 	  = explode('-', $tanggal);
	$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	
	if ($cetak_hari) {
		$num = date('N', strtotime($tanggal));
		return $hari[$num] . ', ' . $tgl_indo;
	}
	return $tgl_indo;
}
?>