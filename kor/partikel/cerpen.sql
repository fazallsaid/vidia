-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 01 Mar 2020 pada 16.08
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cerpen`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
`id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Slice Of Life'),
(2, 'Komedi'),
(3, 'Renungan Hidup');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komentar`
--

CREATE TABLE IF NOT EXISTS `komentar` (
`id_komentar` int(11) NOT NULL,
  `id_story` int(11) NOT NULL,
  `komentar` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `komentar`
--

INSERT INTO `komentar` (`id_komentar`, `id_story`, `komentar`) VALUES
(1, 1, 'Wow keren banget');

-- --------------------------------------------------------

--
-- Struktur dari tabel `story`
--

CREATE TABLE IF NOT EXISTS `story` (
`id_story` int(11) NOT NULL,
  `judul_story` varchar(50) NOT NULL,
  `isi_story` text NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `tgl_post` date NOT NULL,
  `dibaca` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `story`
--

INSERT INTO `story` (`id_story`, `judul_story`, `isi_story`, `id_kategori`, `tgl_post`, `dibaca`) VALUES
(1, 'Belajar Bersyukur', '<p>\r\nHai nama aku Fida,  aku lahir dikota kecil Pacitan Jawa Timur. Kota terindah sepanjang hidupku.  Tapi yang belum pernah denger Kota ini pasti terasa asing. Pacitan adalah kota yang dijuluki "Seribu Satu Goa", "Pacitan Paradise Of Java" dan masih banyak lainnya. Karena di Pacitan itu berasa seperti serba ada.  Yaa, Goa Indah dengan batu-batu yang kokoh,  Pantai yang selalu bikin santai ketika mengunjunginya, dan Bukit-bukit manis yang diatasnya itu kita bisa melihat luasnya kota Pacitan. Pacitan itu kota yang bikin orang-orang merasa merindukan kehangatannya, ke-syahduannya,  dan kenyamanannya. <br/>\r\nEhh ehh kenapa bahas kota ya... Yah sekalian promosi siapa tahu kalian mau Weekend an di Pacitan penuh Kerinduan ini :)\r\n<br/><br/>\r\nEmm.... Umm..... <br/>\r\nAku mau cerita sedikit tentang perjalanan hidupku nih. Aku yakin kalian yang sedang baca cerita ini bakal lebih bersyukur dalam menjalani hidup,  lebih memahami, lebih mencintai apa yang sekarang sedang berjalan sesuai Takdir-Nya,  lebih banyak bergerak daripada mengeluh,  dan lebih banyak pasrah daripada marah. <br/> \r\nA1-23 Th yang Lalu,  aku dilahirkan di dunia oleh seorang ibu yang begitu menyayangi ku, yang menunggu ku untuk segera dewasa agar bisa melihat bagaimana rupa ku.  Apakah akan mirip dengannya? Atau malah mirip dengan Ayahku?. Ibu dan Ayahku adalah dua makhluk spesial untukku,  mereka tidak akan pernah tergantikan selamanya. Aku menyayanginya tapi entah kenapa semakin tumbuh dewasanya aku,  mengungkapkan perasaan itu didepannya langsungpun merasa malu dan canggung. Aku Putri Tunggal dari Ayah dan Ibuku.  jadi anak tunggal itu seperti apa-apa sendiri dan lebih merasa kesepian.  Tapi,  Alhamdulillah Ayah dan Ibuku adalah orang tua paling keren menurut versiku. Mereka itu masih merasa seperti ABG, kadang tuh,  kalo aku tibatiba diem ga jelas selalu dibecandain dengan bahasa gaulnya "Hei,kamu anak muda kenapa wajah udah jelek di jelek-jelekin.  Berterus terang lah kamu anak muda...!! " . Intinya,  mereka tidak mau kehilangan Tawa ku dan selalu ingin mendengar cerita setiap hari ku. ahh  Aku mencintainya... <br/>\r\nAku ingin mereka tersenyum melihat kesuksesanku di masa depan. Di wajah keriput dan mata lelah mereka,  aku ingin membayar segalanya yang mereka perjuangkan untukku. Aku ingin membahagiakannya!. <br/>\r\nA-2 Di umur yg sudah mendekati masa-masa cocok untuk Menikah, aku malah masih menganggur dan klontang -klantung mencari pekerjaan kesana kesini.  Menunggu panggilan berhari-hari.  Berharap Tuhan memberi Keajaiban tentang jalan nasibku.  Penyesalan ada,  banyak pertanyaan dalam diri ini mulai dari "Kenapa aku tidak memulai kerja dari waktu Kuliah saja", "Ampunn nyari kerjaan susah sekali,  apa aku emang dikasih jalan kayak gini". Perasaan putus asa, menyerah,lelah dan malu dengan teman yang seumuranku yang sudah lebih baik daripada hidupku. Iri,  ya iri adalah sifat mengerikan dalam diri ini,  melihat semua orang begitu mudah mewujudkan impiannya,  sedangkan aku?  Aku selalu di urutan paling ujung terakhir dalam mengejar karir.  Kenapa aku tidak bisa sama dengan mereka?  Apa yang membedakanku dengan mereka?  . Begitu banyak pertanyaan seperti itu muncul di Otakku yang serba pas-pas an ini. \r\n<br/>\r\nA3- Aku merasa malu dan sedih untuk menatap mata Ayah dan Ibu.  Mereka masih banting tulang untuk kehidupanku di usia detik-detik menua nya.  Ayahku bekerja sebagai Staff TU di Universitas Merdeka yang tidak begitu jauh dari rumah, yang sebentar lagi akan pensiun. Sebab ayah sudah berumur 50Tahun. Ibuku bekerja sebagai Tukang Jahit di rumah.  Kadang kalau Ibu sedang sepi tidak ada jahitan ia selalu menggambar desain-desain baju.  Dulu ibuku punya cita-cita jadi desainer.  Tapi karena  ekonomi keluarga ibu tidak mencukupi akhirnya ibu hanya mengikuti Balai Latihan Kerja di Bidang Menjahit. Dan aku,  aku masih tetap dirumah menunggu panggilan kerja, keluar rumah ngelamar kerja sana-sini,  sharing dengan temen soal lowongan kerja.  Membosankan! <br/>\r\nAku lulusan Sarjana Pendidikan Guru Sekolah Dasar di Universitas swasta di Pacitan.  Dengan Ipk Cumlaude.  Lulusan terbaik,  tapi masih belum ada panggilan kerja.  Hampir setiap hari memasukan lamaran Guru SD dimanapun.  Waktu itu,  sama ayah disuruh ikut kerja di kampus tempat ayah kerja,  jadi pengganti staff TU  bagian administrasi,  karena yang ngurusin sebelumnya sedang ambil cuti melahirkan. Aku mencoba karena kalau aku tidak mencoba,  aku tidak akan bisa dan tidak akan berani untuk mencoba hal baru. Karena kadang passion kerja itu bisa beda dari Fakultas yang ditekuni. Ya ngga? <br/>\r\n3 Bulan aku jadi pengganti di kampus ayah,  aku dapat kerjaan dari teman ku.  Namanya afi. Dia memberiku lowongan kerja sebagai Guru Les khusus SD kelas 1-6. Tanpa ragu,  langsung aku ambil.  Hari pertama kerja jadi guru les itu begitu berat karena sebelumnya belum pernah ini bibir ngomong terus dari jam setengah 1 siang sampai jam 8 malam.  Hehe.  Ya,  jadwal mengajar les itu ada 5 sesi.  Mulai dari jam setengah 1 hingga jam 8 malam.  Masing-masing pertemuan itu satu setengah jam. Apalagi kalau sudah musim Ujian Sekolah.  Pasti semua murid minta les setiap hari. <br/>\r\nA-4 Apapun aku syukuri,  karena itu bisa mendapatkan pengalaman.  Lama kelamaan jadi guru les menjadi menyenangkan, dan bisa menghilangkan kata "nganggur". Ayah dan Ibu selalu mengingatkan,  "Kerja jangan cuma niat cari duit nduk diimbangi niat nyari pengalaman siapa tahu ini jadi jalan suksesmu." "Kerja jangan asal-asalan apalagi itu berurusan dengan belajar mengajar ya nduk,  harus rapi dan jangan merugikan orang lain. "  Mereka adalah penyemangat sepanjang hidup.  Setiap gajian tidak lupa selalu menyisihkan untuk mereka.  Entah membantu kebutuhan dapur ibu,  membantu ayah bayar listrik dan sebagainya.  Percaya ngga sih,  Gaji itu walaupun kelihatan sedikit tapi kalau kita mau bersyukur dan berbagi tidak akan pernah merasa kurang,  malah merasa setelah itu selalu ada aja rezeki dari arah yang tidak disangka. Lewat hal itu,  aku merasa lega bisa sedikit meringankan orang tua.  Mereka mengajariku sabar dalam berharap.  Ibu dan ayah tidak pernah malu ketika aku menganggur lama. Ketika anak tetangga sudah bekerja di perusahaan ternama, bisa beli ini itu.  Ayah dan Ibu memberi aku support agar tetap optimis dan selalu berusaha. Bagi ayah dan ibu, membandingkan anak sendiri dengan orang lain adalah hal konyol. Karena kemampuan orang itu pasti berbeda. Aku mengagumimu ayah, ibu :) <br/>\r\nA-5 berbulan-bulan telah berlalu,  tepat di bulan september ada info bahwa akan dibuka Pendaftaran Guru Sekolah Dasar Negeri 02 Pacitan. SD Negeri 02 di Pacitan ini adalah SD terbaik. Guru-Guru nya pun ada yang dari luar negeri.  SD nya begitu mewah dan keren. Woah.  Dalam hati,  aku ingin mencoba. Memberi semangat untuk diri sendiri. Aku mulai mendaftar dan lolos seleksi administrasi. Kemudian lanjut Tes. Tes itu diadakan di Ponorogo.  Karena SD Negeri 02 ini bekerjasama dengan SD Terbaik juga di Ponorogo. Ayah sangat ingin mengantarkanku kesana,  menemani aku tes agar lebih semangat. <br/>\r\nWaktu itu,  jam 03:00 pagi aku dan ayah berangkat dari Pacitan ke Ponoroho. Kebetulan aku tes pada jam 08:00 pagi. Cuaca sedang extreme,  hujan turun selama dua hari berturut-turut.  Aku dan ayah nekat pergi naik sepedah motor. Selama diperjalanan ayah selalu mengingatkan Kartu ujianku jangan sampai basah.  Dan aku hanya berucap iya iya dan iya.  Perjalanan sangat mencekam,  terdengar rintikan air hujan, suara air sungai di kanan jalan dansayup sayup pohon dibukit kiri jalan.  Kenapa aku dan ayah tidak menginap saja di Ponorogo?  Karena Aku dan Ayah percaya kalau esok itu hujan akan reda :). Aneh sih.  Tapi ya gitu adanya. \r\nA-6. Sampailah jam 05:30 pagi di Ponorogo Istirahat di masjid sekalian sholat shubuh.  Ayah langsung segera menelpon ibu untuk mengabari bahwa sudah sampai di Ponorogo dengan Selamat. Aku persiapan ganti baju Atasan Putih dan Bawah Hitam. Tepat pukul 06:30 sudah berada di lokasi tes.  Aku segera mengambil kartu ujian yang berada di tas.  Dannnnn,  apa yang terjadi..  Kartu ujian basah.  Tidak bawa flashdisk untuk print ulang.  Nangiss sejadi-jadinya di depan pos satpam. Marah lah ayah.  "Tadi kan ayah sudah bilang,  Kartu ujian jgn basah. Ayo di cek lagi. Malah bilang iya.  Terus bagaimana ini?". Dan aku hanya menangis , meminta maaf kepada ayah. Maklum ya,  ini baru pertama ikut tes kayak gini dan kaget bisa melangkah sejauh ini jadi jangan sampai ini sia-sia. makanya bingung dan takut jadi satu. Lalu ayah bilang "sudah nduk,  masuk saja coba ditanyakan apa ini masih berlaku ini yang dibutuhin koyok e barcode nya.  Barcodenya masih aman. Bismilah nduk kalo ndak bisa ndak papa,  ayah tunggu di luar. Kita pergi jalan-jalan saja disini. " Hancurr hati aku seketika,  kenapa kesalahan ini!?. Setelah pengorbanan yang sudah dilewati. <br/>\r\nAkhirnya,  aku berani masuk ke tempat ujian dan bertanya kepada panitia.  Alhamdulillah ada jalan keluar. Bisa di print ulang di ruangan yang telah di sediakan.  Disitu rasa syukur tidak ada hentinya,  dalam hati shalawat terus padahal otak sudah blank,  sudah tidak fokus dan tahan nangis.  Dalam hati Jika hari ini adalah Hariku,  aku akan lebih banyak bersyukur. Jika hari ini bukan hariku,  aku tahu Alloh akan menyiapkan yang terbaik esok hari. Intinya disitu aku Pasrah. <br/>\r\nA7 Tepat jam 09:30 Tes telah selesai.  Disitu langsung bisa dilihat nilainya.  Aku duduk di tangga masih meratapi kesalahanku. Tidak berharap akan lolos passing grade karena merasa soal-soal yang ku kerjakan tadi ku jawab setahuku saja dan Micro teaching (Latihan Mengajar) yang ku lakukan tadi hanyalah seingat ku dulu waktu PPL (Praktek Pengalaman Lapangan). Oh iya,  passing grade nya 500.Tidak lama itu,  aku kembali berharap. Membisikian ke diri sendiri "Ayo kamu semangat fid! Jangan nyerah. Kalo gagal emang bukan rezekimu . Ayo bangun! " begitulah gumamku. Karena ini adalah tes yang menentukan berhasil tidaknya untuk pendaftaran Sebagai Guru SD N 02 Pacitan. Aku melihat hasil tes ku,  apa yang terjadi?  Pertolongan Alloh selalu datang tepat pada waktunya.  500 adalah nilai tes ku. Sudah pas dengan Passing Grade.  Menangis lagi sejadinya. <br/>\r\nAku berlari menemui ayah di ruang tunggu,  aku memeluk erat dan menangis lagi.  Lalu ayah bilang "kenapa nduk?  Jangan nangis udah gede lho.  Kalo gagal ndak papa.  Bukan rezekimu nduk. Ayah juga seneng karena dulu ayah ndak pernah ngerasain tes seperti ini. Setidaknya anak ayah sudah mengalami.  Ndak papa nduk" . Masih dalam pelukan ayah aku membisikan kata kepadanya "Ayah,  terimkasih sudah membuatku percaya diri. Alhamdulillah ayah,  aku lolos". Spontan kan ayah aku langsung bilang "Alhamdulillah!!" sambil melepaskan pelukanku. Dan buru-buru memberi kabar ibu.  Kita pun akhirnya bergegas pulang ke pacitan. Dengan perasaan lega dan bersyukur. <br/>\r\nA8 Sampai di Rumah,  Ayah mulai bikin bercandaan ke ibu.  Ayah bilang "bune, ayah tadi ndak bisa ngerjain soalnya.  Soalnya yang ngerjain ayah bune. Aduh ayah ndak lolos bune". Yahh gitu lah ayahku,  selalu bikin suasana mellow jadi komedi.  Seketika terdengar tawa renyah ayah dan ibu.  Ibu langsung memeluk ku,  dan bersyukur.  Akhirnya apa yang di doa kan ibu dan ayah terwujud. Ibu menasehati ku "nduk, kamu sudah kerja keras. Ibu bangga nduk. Apapun itu di syukuri jangan sombong kalau sudah di atas.  Roda berputar nduk. Kamu ya harus ingat waktu susah mu. Jangan terlalu bangga, jangan terlalu senang. Karena semua harus seimbang ya nduk". Huhu. Nangis lagi dipelukan ibu. <br/>\r\nHari-hari berlalu, aku sudah berstatus menjadi Guru Tetap di SD Negeri 02 Pacitan.  SD Negeri Terbaik nomor dua di Pacitan.  Seperti para pekerja lainnya,  berangkat pagi dan pulang sore. Alhamdulillah. <br/>\r\nA9 jangan menyerah hanya karena hdiup kita berbeda. Kita sama sama berjuang tapi hanya beda jalan.  Kita sama-sama jatuh hanya beda tempat.  Kita sama-sama terluka hanya beda cara mengobati. Jangan berhenti jika dirasa itu tidak mungkin.  Kamu percaya "Kun Fayakun". Maka semua yang tidak mungkin akan mungkin terjadi.  Proses Tidak ada yang mengkhianati. Jika gagal,  itu adalah cara agar kita tumbuh lebih kuat.  Bukankah semakin kuat pohon, akan semakin berat angin menjatuhkan?. <br/> \r\nTetap berpegang pada Ridho Alloh,  yaitu Ridho orang tua.  Mereka adalah manusia yang menghargai kerja kerasmu dibanding lainnya.  Bahagiakan mereka, hingga akhir hayatnya. Walaupun sudah tidak ada pun, kamu harus bisa membuatnya lebih bahagia dan bersyukur karena kamu pernah menjadi alasannya untuk tersenyum. \r\n</p>', 1, '2020-02-29', 31);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
 ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
 ADD PRIMARY KEY (`id_komentar`), ADD KEY `id_story` (`id_story`);

--
-- Indexes for table `story`
--
ALTER TABLE `story`
 ADD PRIMARY KEY (`id_story`), ADD KEY `id_kategori` (`id_kategori`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
MODIFY `id_komentar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `story`
--
ALTER TABLE `story`
MODIFY `id_story` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `komentar`
--
ALTER TABLE `komentar`
ADD CONSTRAINT `komentar_ibfk_1` FOREIGN KEY (`id_story`) REFERENCES `story` (`id_story`);

--
-- Ketidakleluasaan untuk tabel `story`
--
ALTER TABLE `story`
ADD CONSTRAINT `story_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
