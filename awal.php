<div class="col-xl-8 py-5 px-md-5">
						<div class="row pt-md-4">
			    			<div class="col-md-12">
							<center>Pilih Bahasa/언어 선택
							<br/>
							<a href="./"><img src="images/id.png" /></a> / <a href="kor/"><img src="images/kor.png" /></a>
							</center>
							</div>
			    		</div>
						<hr/>
					<?php
						$z = mysqli_query($connection, "SELECT story.*, kategori.*
						FROM story
						JOIN kategori ON kategori.id_kategori=story.id_kategori WHERE status='P'
						ORDER BY id_story DESC");
						while ($ass = mysqli_fetch_array($z)): 
						?>
	    				<div class="row pt-md-4">
			    			<div class="col-md-12">
							<div class="blog-entry ftco-animate d-md-flex">
										<a href="?page=detailpost&id_story=<?php echo $ass['id_story']; ?>" class="img img-2" style="background-image: url(images/thumbnaila.jpg);"></a>
										<div class="text text-2 pl-md-4">
				              <h3 class="mb-2"><a href="?page=detailpost&id_story=<?php echo $ass['id_story']; ?>"><?php echo $ass['judul_story']; ?></a></h3>
				              <div class="meta-wrap">
												<p class="meta">
				              		<span><i class="icon-calendar mr-2"></i><?php echo tanggal_indo($ass['tgl_post']); ?></span>
				              		<span><i class="icon-folder-o mr-2"></i><?php echo $ass['nama_kategori']; ?></span>
									  <span><i class="icon-eye mr-2"></i><?php echo $ass['dibaca']; ?>x</span>
				              	</p>
			              	</div>
				              <p class="mb-4"><?php echo substr($ass['isi_story'],0,140); ?>...</p>
				              <p><a href="?page=detailpost&id_story=<?php echo $ass['id_story']; ?>" class="btn-custom">Selengkapnya <span class="ion-ios-arrow-forward"></span></a></p>
				            </div>
							</div>
							</div>
			    		</div>
						<?php endwhile; ?>
						<!-- END-->
			    	
			    	</div>