<div class="row justify-content-center">
        <div class=" col ">
          <div class="card">
            <div class="card-header bg-transparent">
              <h3 class="mb-0">Anda menemukan halaman 404 kami!</h3>
            </div>
            <div class="card-body">
              Maaf, sepertinya halaman yang Anda tuju tidak ditemukan.
            </div>
          </div>
        </div>
      </div>