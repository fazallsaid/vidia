-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Mar 2020 pada 06.47
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cerpen`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `uname` varchar(15) NOT NULL,
  `password` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `uname`, `password`, `nama`) VALUES
(1, 'admin', 'admin', 'Vidia Ardiyanti');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Slice Of Life'),
(2, 'Komedi'),
(3, 'Renungan Hidup');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komentar`
--

CREATE TABLE `komentar` (
  `id_komentar` int(11) NOT NULL,
  `id_story` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `komentarnya` text NOT NULL,
  `tgl_komen` date NOT NULL,
  `baca` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `komentar`
--

INSERT INTO `komentar` (`id_komentar`, `id_story`, `nama`, `komentarnya`, `tgl_komen`, `baca`) VALUES
(7, 1, 'fazal', '<p>Bales Oyyy</p>', '2020-03-03', 'Y'),
(8, 1, 'fazal', '<p>tes komen ajax</p>', '2020-03-03', 'Y'),
(9, 1, 'fazal', '<p>tes lagi</p>', '2020-03-03', 'Y'),
(12, 2, 'Fazal Said', '<p>cie cerpen baru lagi</p>', '2020-03-05', 'Y'),
(13, 4, 'Fazal Said', '<p>Wow sarjana?</p>', '2020-03-05', 'Y'),
(14, 2, 'fazal', '<p>Semangat 30 views!</p>', '2020-03-05', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `story`
--

CREATE TABLE `story` (
  `id_story` int(11) NOT NULL,
  `judul_story` varchar(50) NOT NULL,
  `isi_story` text NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `tgl_post` date NOT NULL,
  `dibaca` int(11) NOT NULL,
  `status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `story`
--

INSERT INTO `story` (`id_story`, `judul_story`, `isi_story`, `id_kategori`, `tgl_post`, `dibaca`, `status`) VALUES
(1, 'Syukur Menjadikan Makmur', '<p>\r\nHai nama aku Fida,  aku lahir dikota kecil Pacitan Jawa Timur. Kota terindah sepanjang hidupku.  Tapi yang belum pernah denger Kota ini pasti terasa asing. Pacitan adalah kota yang dijuluki \"Seribu Satu Goa\", \"Pacitan Paradise Of Java\" dan masih banyak lainnya. Karena di Pacitan itu berasa seperti serba ada.  Yaa, Goa Indah dengan batu-batu yang kokoh,  Pantai yang selalu bikin santai ketika mengunjunginya, dan Bukit-bukit manis yang diatasnya itu kita bisa melihat luasnya kota Pacitan. Pacitan itu kota yang bikin orang-orang merasa merindukan kehangatannya, ke-syahduannya,  dan kenyamanannya. <br/>\r\nEhh ehh kenapa bahas kota ya... Yah sekalian promosi siapa tahu kalian mau Weekend an di Pacitan penuh Kerinduan ini :)\r\n<br/><br/>\r\nEmm.... Umm..... <br/>\r\nAku mau cerita sedikit tentang perjalanan hidupku nih. Aku yakin kalian yang sedang baca cerita ini bakal lebih bersyukur dalam menjalani hidup,  lebih memahami, lebih mencintai apa yang sekarang sedang berjalan sesuai Takdir-Nya,  lebih banyak bergerak daripada mengeluh,  dan lebih banyak pasrah daripada marah. <br/> \r\nA1-23 Th yang Lalu,  aku dilahirkan di dunia oleh seorang ibu yang begitu menyayangi ku, yang menunggu ku untuk segera dewasa agar bisa melihat bagaimana rupa ku.  Apakah akan mirip dengannya? Atau malah mirip dengan Ayahku?. Ibu dan Ayahku adalah dua makhluk spesial untukku,  mereka tidak akan pernah tergantikan selamanya. Aku menyayanginya tapi entah kenapa semakin tumbuh dewasanya aku,  mengungkapkan perasaan itu didepannya langsungpun merasa malu dan canggung. Aku Putri Tunggal dari Ayah dan Ibuku.  jadi anak tunggal itu seperti apa-apa sendiri dan lebih merasa kesepian.  Tapi,  Alhamdulillah Ayah dan Ibuku adalah orang tua paling keren menurut versiku. Mereka itu masih merasa seperti ABG, kadang tuh,  kalo aku tibatiba diem ga jelas selalu dibecandain dengan bahasa gaulnya \"Hei,kamu anak muda kenapa wajah udah jelek di jelek-jelekin.  Berterus terang lah kamu anak muda...!! \" . Intinya,  mereka tidak mau kehilangan Tawa ku dan selalu ingin mendengar cerita setiap hari ku. ahh  Aku mencintainya... <br/>\r\nAku ingin mereka tersenyum melihat kesuksesanku di masa depan. Di wajah keriput dan mata lelah mereka,  aku ingin membayar segalanya yang mereka perjuangkan untukku. Aku ingin membahagiakannya!. <br/>\r\nA-2 Di umur yg sudah mendekati masa-masa cocok untuk Menikah, aku malah masih menganggur dan klontang -klantung mencari pekerjaan kesana kesini.  Menunggu panggilan berhari-hari.  Berharap Tuhan memberi Keajaiban tentang jalan nasibku.  Penyesalan ada,  banyak pertanyaan dalam diri ini mulai dari \"Kenapa aku tidak memulai kerja dari waktu Kuliah saja\", \"Ampunn nyari kerjaan susah sekali,  apa aku emang dikasih jalan kayak gini\". Perasaan putus asa, menyerah,lelah dan malu dengan teman yang seumuranku yang sudah lebih baik daripada hidupku. Iri,  ya iri adalah sifat mengerikan dalam diri ini,  melihat semua orang begitu mudah mewujudkan impiannya,  sedangkan aku?  Aku selalu di urutan paling ujung terakhir dalam mengejar karir.  Kenapa aku tidak bisa sama dengan mereka?  Apa yang membedakanku dengan mereka?  . Begitu banyak pertanyaan seperti itu muncul di Otakku yang serba pas-pas an ini. \r\n<br/>\r\nA3- Aku merasa malu dan sedih untuk menatap mata Ayah dan Ibu.  Mereka masih banting tulang untuk kehidupanku di usia detik-detik menua nya.  Ayahku bekerja sebagai Staff TU di Universitas Merdeka yang tidak begitu jauh dari rumah, yang sebentar lagi akan pensiun. Sebab ayah sudah berumur 50Tahun. Ibuku bekerja sebagai Tukang Jahit di rumah.  Kadang kalau Ibu sedang sepi tidak ada jahitan ia selalu menggambar desain-desain baju.  Dulu ibuku punya cita-cita jadi desainer.  Tapi karena  ekonomi keluarga ibu tidak mencukupi akhirnya ibu hanya mengikuti Balai Latihan Kerja di Bidang Menjahit. Dan aku,  aku masih tetap dirumah menunggu panggilan kerja, keluar rumah ngelamar kerja sana-sini,  sharing dengan temen soal lowongan kerja.  Membosankan! <br/>\r\nAku lulusan Sarjana Pendidikan Guru Sekolah Dasar di Universitas swasta di Pacitan.  Dengan Ipk Cumlaude.  Lulusan terbaik,  tapi masih belum ada panggilan kerja.  Hampir setiap hari memasukan lamaran Guru SD dimanapun.  Waktu itu,  sama ayah disuruh ikut kerja di kampus tempat ayah kerja,  jadi pengganti staff TU  bagian administrasi,  karena yang ngurusin sebelumnya sedang ambil cuti melahirkan. Aku mencoba karena kalau aku tidak mencoba,  aku tidak akan bisa dan tidak akan berani untuk mencoba hal baru. Karena kadang passion kerja itu bisa beda dari Fakultas yang ditekuni. Ya ngga? <br/>\r\n3 Bulan aku jadi pengganti di kampus ayah,  aku dapat kerjaan dari teman ku.  Namanya afi. Dia memberiku lowongan kerja sebagai Guru Les khusus SD kelas 1-6. Tanpa ragu,  langsung aku ambil.  Hari pertama kerja jadi guru les itu begitu berat karena sebelumnya belum pernah ini bibir ngomong terus dari jam setengah 1 siang sampai jam 8 malam.  Hehe.  Ya,  jadwal mengajar les itu ada 5 sesi.  Mulai dari jam setengah 1 hingga jam 8 malam.  Masing-masing pertemuan itu satu setengah jam. Apalagi kalau sudah musim Ujian Sekolah.  Pasti semua murid minta les setiap hari. <br/>\r\nA-4 Apapun aku syukuri,  karena itu bisa mendapatkan pengalaman.  Lama kelamaan jadi guru les menjadi menyenangkan, dan bisa menghilangkan kata \"nganggur\". Ayah dan Ibu selalu mengingatkan,  \"Kerja jangan cuma niat cari duit nduk diimbangi niat nyari pengalaman siapa tahu ini jadi jalan suksesmu.\" \"Kerja jangan asal-asalan apalagi itu berurusan dengan belajar mengajar ya nduk,  harus rapi dan jangan merugikan orang lain. \"  Mereka adalah penyemangat sepanjang hidup.  Setiap gajian tidak lupa selalu menyisihkan untuk mereka.  Entah membantu kebutuhan dapur ibu,  membantu ayah bayar listrik dan sebagainya.  Percaya ngga sih,  Gaji itu walaupun kelihatan sedikit tapi kalau kita mau bersyukur dan berbagi tidak akan pernah merasa kurang,  malah merasa setelah itu selalu ada aja rezeki dari arah yang tidak disangka. Lewat hal itu,  aku merasa lega bisa sedikit meringankan orang tua.  Mereka mengajariku sabar dalam berharap.  Ibu dan ayah tidak pernah malu ketika aku menganggur lama. Ketika anak tetangga sudah bekerja di perusahaan ternama, bisa beli ini itu.  Ayah dan Ibu memberi aku support agar tetap optimis dan selalu berusaha. Bagi ayah dan ibu, membandingkan anak sendiri dengan orang lain adalah hal konyol. Karena kemampuan orang itu pasti berbeda. Aku mengagumimu ayah, ibu :) <br/>\r\nA-5 berbulan-bulan telah berlalu,  tepat di bulan september ada info bahwa akan dibuka Pendaftaran Guru Sekolah Dasar Negeri 02 Pacitan. SD Negeri 02 di Pacitan ini adalah SD terbaik. Guru-Guru nya pun ada yang dari luar negeri.  SD nya begitu mewah dan keren. Woah.  Dalam hati,  aku ingin mencoba. Memberi semangat untuk diri sendiri. Aku mulai mendaftar dan lolos seleksi administrasi. Kemudian lanjut Tes. Tes itu diadakan di Ponorogo.  Karena SD Negeri 02 ini bekerjasama dengan SD Terbaik juga di Ponorogo. Ayah sangat ingin mengantarkanku kesana,  menemani aku tes agar lebih semangat. <br/>\r\nWaktu itu,  jam 03:00 pagi aku dan ayah berangkat dari Pacitan ke Ponoroho. Kebetulan aku tes pada jam 08:00 pagi. Cuaca sedang extreme,  hujan turun selama dua hari berturut-turut.  Aku dan ayah nekat pergi naik sepedah motor. Selama diperjalanan ayah selalu mengingatkan Kartu ujianku jangan sampai basah.  Dan aku hanya berucap iya iya dan iya.  Perjalanan sangat mencekam,  terdengar rintikan air hujan, suara air sungai di kanan jalan dansayup sayup pohon dibukit kiri jalan.  Kenapa aku dan ayah tidak menginap saja di Ponorogo?  Karena Aku dan Ayah percaya kalau esok itu hujan akan reda :). Aneh sih.  Tapi ya gitu adanya. \r\nA-6. Sampailah jam 05:30 pagi di Ponorogo Istirahat di masjid sekalian sholat shubuh.  Ayah langsung segera menelpon ibu untuk mengabari bahwa sudah sampai di Ponorogo dengan Selamat. Aku persiapan ganti baju Atasan Putih dan Bawah Hitam. Tepat pukul 06:30 sudah berada di lokasi tes.  Aku segera mengambil kartu ujian yang berada di tas.  Dannnnn,  apa yang terjadi..  Kartu ujian basah.  Tidak bawa flashdisk untuk print ulang.  Nangiss sejadi-jadinya di depan pos satpam. Marah lah ayah.  \"Tadi kan ayah sudah bilang,  Kartu ujian jgn basah. Ayo di cek lagi. Malah bilang iya.  Terus bagaimana ini?\". Dan aku hanya menangis , meminta maaf kepada ayah. Maklum ya,  ini baru pertama ikut tes kayak gini dan kaget bisa melangkah sejauh ini jadi jangan sampai ini sia-sia. makanya bingung dan takut jadi satu. Lalu ayah bilang \"sudah nduk,  masuk saja coba ditanyakan apa ini masih berlaku ini yang dibutuhin koyok e barcode nya.  Barcodenya masih aman. Bismilah nduk kalo ndak bisa ndak papa,  ayah tunggu di luar. Kita pergi jalan-jalan saja disini. \" Hancurr hati aku seketika,  kenapa kesalahan ini!?. Setelah pengorbanan yang sudah dilewati. <br/>\r\nAkhirnya,  aku berani masuk ke tempat ujian dan bertanya kepada panitia.  Alhamdulillah ada jalan keluar. Bisa di print ulang di ruangan yang telah di sediakan.  Disitu rasa syukur tidak ada hentinya,  dalam hati shalawat terus padahal otak sudah blank,  sudah tidak fokus dan tahan nangis.  Dalam hati Jika hari ini adalah Hariku,  aku akan lebih banyak bersyukur. Jika hari ini bukan hariku,  aku tahu Alloh akan menyiapkan yang terbaik esok hari. Intinya disitu aku Pasrah. <br/>\r\nA7 Tepat jam 09:30 Tes telah selesai.  Disitu langsung bisa dilihat nilainya.  Aku duduk di tangga masih meratapi kesalahanku. Tidak berharap akan lolos passing grade karena merasa soal-soal yang ku kerjakan tadi ku jawab setahuku saja dan Micro teaching (Latihan Mengajar) yang ku lakukan tadi hanyalah seingat ku dulu waktu PPL (Praktek Pengalaman Lapangan). Oh iya,  passing grade nya 500.Tidak lama itu,  aku kembali berharap. Membisikian ke diri sendiri \"Ayo kamu semangat fid! Jangan nyerah. Kalo gagal emang bukan rezekimu . Ayo bangun! \" begitulah gumamku. Karena ini adalah tes yang menentukan berhasil tidaknya untuk pendaftaran Sebagai Guru SD N 02 Pacitan. Aku melihat hasil tes ku,  apa yang terjadi?  Pertolongan Alloh selalu datang tepat pada waktunya.  500 adalah nilai tes ku. Sudah pas dengan Passing Grade.  Menangis lagi sejadinya. <br/>\r\nAku berlari menemui ayah di ruang tunggu,  aku memeluk erat dan menangis lagi.  Lalu ayah bilang \"kenapa nduk?  Jangan nangis udah gede lho.  Kalo gagal ndak papa.  Bukan rezekimu nduk. Ayah juga seneng karena dulu ayah ndak pernah ngerasain tes seperti ini. Setidaknya anak ayah sudah mengalami.  Ndak papa nduk\" . Masih dalam pelukan ayah aku membisikan kata kepadanya \"Ayah,  terimkasih sudah membuatku percaya diri. Alhamdulillah ayah,  aku lolos\". Spontan kan ayah aku langsung bilang \"Alhamdulillah!!\" sambil melepaskan pelukanku. Dan buru-buru memberi kabar ibu.  Kita pun akhirnya bergegas pulang ke pacitan. Dengan perasaan lega dan bersyukur. <br/>\r\nA8 Sampai di Rumah,  Ayah mulai bikin bercandaan ke ibu.  Ayah bilang \"bune, ayah tadi ndak bisa ngerjain soalnya.  Soalnya yang ngerjain ayah bune. Aduh ayah ndak lolos bune\". Yahh gitu lah ayahku,  selalu bikin suasana mellow jadi komedi.  Seketika terdengar tawa renyah ayah dan ibu.  Ibu langsung memeluk ku,  dan bersyukur.  Akhirnya apa yang di doa kan ibu dan ayah terwujud. Ibu menasehati ku \"nduk, kamu sudah kerja keras. Ibu bangga nduk. Apapun itu di syukuri jangan sombong kalau sudah di atas.  Roda berputar nduk. Kamu ya harus ingat waktu susah mu. Jangan terlalu bangga, jangan terlalu senang. Karena semua harus seimbang ya nduk\". Huhu. Nangis lagi dipelukan ibu. <br/>\r\nHari-hari berlalu, aku sudah berstatus menjadi Guru Tetap di SD Negeri 02 Pacitan.  SD Negeri Terbaik nomor dua di Pacitan.  Seperti para pekerja lainnya,  berangkat pagi dan pulang sore. Alhamdulillah. <br/>\r\nA9 jangan menyerah hanya karena hdiup kita berbeda. Kita sama sama berjuang tapi hanya beda jalan.  Kita sama-sama jatuh hanya beda tempat.  Kita sama-sama terluka hanya beda cara mengobati. Jangan berhenti jika dirasa itu tidak mungkin.  Kamu percaya \"Kun Fayakun\". Maka semua yang tidak mungkin akan mungkin terjadi.  Proses Tidak ada yang mengkhianati. Jika gagal,  itu adalah cara agar kita tumbuh lebih kuat.  Bukankah semakin kuat pohon, akan semakin berat angin menjatuhkan?. <br/> \r\nTetap berpegang pada Ridho Alloh,  yaitu Ridho orang tua.  Mereka adalah manusia yang menghargai kerja kerasmu dibanding lainnya.  Bahagiakan mereka, hingga akhir hayatnya. Walaupun sudah tidak ada pun, kamu harus bisa membuatnya lebih bahagia dan bersyukur karena kamu pernah menjadi alasannya untuk tersenyum. \r\n</p>', 1, '2020-02-29', 83, 'P'),
(2, 'Mampukah kau menjadi aku?', '<p>Terkadang aku melihat hidup orang lain&nbsp; merasa iri, merasa bahwa kenapa bukan aku saja yang hidupnya seperti itu, yang bermewah-mewahan, yang dicintai sekelilingnya dan yang di rindukan setiap harinya. Ketika aku telah dewasa dan &nbsp;mengenal hidupku, dengan kehidupan&nbsp; yang serba cukup, keluarga yang broken home dan memiliki saudara laki-laki. Aku merasa bahwa inilah hukum alam, yang seharusnya aku terima. Tepat dua tahun yang lalu adalah dimana aku menginjak&nbsp;bangku kuliah, dengan pilihan kampus yang sudah ku idam-idamkan sejak SMA sebut saja &ldquo;kampus sebelah&rdquo;. Dikampus tersebut ak benar-benar mencintai seluruh isi dan metode pembelajarannya serta kemewahan dan kefavoritan dari kampus tersebut. 6 juni 2016 aku telah diterima di&rdquo;kampus sebelah&rdquo; betapa bahagianya aku dengan kemantapan hati, rasa bersyukur dan kerja keras aku mampu mengerjakan soal-soal tes dengan nilai terbaik, namun, mampukah kau&nbsp; menjadi aku? Merasakan bahwa kau benar-benar dinyatakan tidak bisa untuk belajar&nbsp; disana dengan kendala biaya? Bahkan orang tua dari temanmu yang dibilang dari keluarga miskin berusaha menjual apapun agar temanu itu&nbsp; bisa belajar di &ldquo;kampus sebelah&rdquo; dan membanggakan nama orang tuanya, sedangkan orang tuamu tidak berusaha mencarikan uang demi kau padahal orang tua mu adalah orang yang mampu ? mampukah kau merasakan menjadi aku? Setelah kau&nbsp; mengidam-idamkan untuk belajar di kampus itu dan kamu berhasil menjalani semua tes, namun kau tidak di izinkan untuk&nbsp; belajar disana hanya karena biaya?. Dulu pembayaran dikampus sebelah berkisar 5 juta-10juta untuk satu semester dengan fakultas manajemen ekonomi, ya aku memilih fakultas tersebut&nbsp; karena waktu SMA aku masuk di penjurusan IPS ekonomi, aku banyak belajar tentang teori ekonomi dan yakin bahwa itu adalah ilmu modern dan selalu mengikuti zaman serta aku sangat mencintai ekonomi.</p><p>Selama beberapa hari aku memikirkan bagaimana aku bisa merayu orang tua ku agar membiayai kuliahku dengan nominal sebanyak itu? Apakah aku harus benar-benar merelakan semua impianku? Apakah aku benar-benar harus melupakan &ldquo;Kampus Sebelah&rdquo; dan beralih mencari kampus lagi? &ldquo;Ahh sudahlah sebaiknya aku menunggu satu tahun saja untuk belajar lagi dan aku akan bekerja saja&rdquo; gumamku. Namun akhirnya, aku dipaksa untuk belajar di kampus yang daerahnya sangat tidak pernah terbesit dalam pikiranku, begitu jauh. sebut saja &ldquo;kampus jauh&rdquo;. Coba saja bayangkan, disaat kau ingin belajar di &ldquo;kampus sebelah&rdquo; dan disana kamu sudah mempersiapkan semua tentang masa depanmu lalu kau dipaksa untuk belajar di &ldquo;kampus jauh&rdquo; sakit kah? Ingin menangis? Ya, tidak hanya ingin bahkan aku sudah menangis detik ini.</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Aku belajar menerima , belajar mengikhlaskan apa yang terjadi dan aku memulai dengan niat tulus. Sudah hampir selesei aku belajar di &ldquo;kampus jauh&rdquo; bertemu teman-teman yang mungkin aku tidak akan menemukan di &ldquo;kampus sebelah&rdquo;, teman-teman yang kuat, teman-teman yang kompak dan selalu memberi dukungan. &ldquo;Tidak terlalu buruk juga&rdquo; pikirku. Di kampus jauh itu aku mengambil fakultas ekonomi juga,&nbsp; setiap ujian nilai-nilaiku selalu yang terbaik. Mulai saat itu aku berfikir untuk menjadi hebat dan cerdas ternyata bukan karena nama dan keistimewaan kampus. Dan saat itu juga, aku merelakan apa yang telah aku idamkan.</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*****</p><p>Setahun setelah kejadian tersebut, adikku noval namanya juga menginjak di bangku kuliah, dia bersikeras mendaftar sebagai prajurit tentara, setiap perjalanannya setiap pembiayaannya orang tua ku selalu mengikuti dan mampu mengeluarkan biaya bahkan orang tua ku yang telah berpisah sejak aku SMA tersebut membuat perjanjian patungan biaya. Bayangkan saja untuk menjadi prajurit tentara orang tua mu harus mengeluarkan berpuluh-puluh juta setiap waktu yang di butuhkan. Dan untuk membiayai pendaftaran kuliahku di &ldquo;kampus sebelah&rdquo; saja mereka memarahiku lalu memindahkanku. Aahh memang benarr semua sudah terskenario dengan baik, aku percaya bahwa ALLOH masih sayang kepadaku dan menginginkanku untuk merasa biasa dan tetap ceria.</p><p>Mampukah kau menjadi aku? Walaupun diluar sana banyak yang tiddak bisa kuliah atau bahkan memendam keinginan belajarnya di bangku kuliah dan bekerja sebagai tulang punggung keluarga, aku akan tetap bersyukur belajar di &ldquo;kampus jauh&rdquo; ini, dengan kesungguhan hati dan aku ingin menjadi terbaik dari yang terbaik walaupun tidak ada yang terbaik tetapi aku akan menjadi. Dengan cerita kehidupanku ini, aku merasa bahwa apapun bisa diatasi dengan rasa syukur, rasa terimaksih walaupun itu menyakitkan. Dan aku harus membuang sikap iri terhadap orang lain, menutupi semua kesedihan yang terlukis di wajahku dengan senyuman ikhlas. Mampukah kau menjadi aku, setelah kau tau betapa kerasnya kehidupan ini? . jika kau anggap hidup ini keras, maka perbanyaklah belajar memaknai hukum alam. Namun, jangan pernah membenci orang tua setipis rambut pun bagaimanapun ia memperlakukanmu, mereka adalah alasan mengapa kita hidup dan mencari tujuan hidup yang sebenarnya.&nbsp;</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;****</p>', 1, '2020-03-05', 21, 'P'),
(3, 'Berteman itu,jangan berlebihan...', '<p>Pernah kah kau merasa ada kejanggalan dengan sesuatu yang berlebihan? Emm, disini aku akan bercerita tentang &ldquo;berlebih-lebihan&rdquo; , dari kehidupan nyata aku mengambil pelajaran bahwa ketika aku memiliki teman yang sejak TK hingga sekarang selalu bersama, kemanapun bersama, makan, bermain semua bersama bahkan sehari tidak bertemu dunia terasa sepi, dia adalah nisa . tanpa dia aku tidak bisa menjadi seperti ini, menjadi seorang yang kuat karena dia selalau menyemangatiku ketika aku terjatuh.</p><p>suatu hari aku dan temanku diuji dengan kesalahfahaman entah aku tak tau darimana muncul masalah tersebut, tiba-tiba saja nisa menjauhiku, semua foto bersamaku di sosmednya dihapus dan ada status yang bagiku adalah sindiran &ldquo;tak disangka teman sejak kecil tega mengkhianatiku...&rdquo;.&nbsp; &ldquo;hah? Apakah yang dimaksud nisa itu adalah aku&rdquo; pikirku. Tetapi sejak itu ak membiarkan nisa menjauhiku disela itu aku mencari kesalahanku, aku berkhianat apa kepadanya dan aku melakukan apa kepadanya hingga dia begitu benci kepadaku. &ldquo;Aah sepertinya aku tidak melakukan kesalahan apapun, aku tidak berkhianat dalam hal apapun juga, sepertinya ada yang tidak beres... &ldquo;gumamku.</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ***</p><p>Waktu itu di toko sebelah aku bertemu nisa sedang belanja dan aku berusaha menemuinya dan menanyakan langsung kenapa dia begini. &ldquo;Nis, aku salah apa padamu? Aku berkhianat apa padamu? Mari kita selesaikan masalah ini baik-baik..&rdquo; begitu aku bertanya, nisa menjawab dengan tetesan air mata bahwa aku sudah mengungkapkan seemua baik buruknya sifat nisa kepada teman lainnya lalu dia pergi begitu saja. Sontak aku kaget, padahal aku tidak pernah melakukan hal jahat seperti itu, sama saja aku mengumbar aib temanku. Semakin keras aku berpikir, siapa yang tega memutuskan pertemananku dengan nisa?, apakah dia yang merusak itu tak tahu berapa lama aku berteman dengan nisa? Apakah dia yang merusak juga tak tahu sedekat apa aku dan nisa bahkan sudah sampai seperti saudara?.</p><p>Suatu hari aku pergi kerumah nisa, belum sampai masuk rumah nisa aku melihat nisa sedang tertawa dan gembira dengan Arif diteras rumahnya, dia adalah temanku juga sejak SD. Aku tak tahu apa yang mereka bicarakan dan kenapa mereka bisa bertemu.&nbsp; Disitu aku mendengarkan percakapan mereka, betapa kagetnya aku apa yang mereka bicarakan. &ldquo;Rif, akhirnya aku sudah tidak berteman lagi dengan nanda. Hahha..iya berkat cara mu...&rdquo; ungkap nisa. Aku pergi dengan hati yang begitu marah, ternyata ini adalah drama yang dilakukan nisa agar tidak berteman denganku lagi, lalu apa maksud dari ini semua. Tak lama waktu berselang, nisa menghubungiku dia ingin bertemu denganku dan ingin membicarakan sesuatu, dan aku bergegas menemuinya, mendengarkan apa yang akan dia bicarakan.</p><p>Deburan ombak dan sepoinya angin beriringan menyapu dedauan yang jatuh dibibir pantai, sejam aku menunggu nisa akhirnya dia datang, tanpa baasa-basi dia berbicara kepadaku. Dia mulai menjelaskan pelan-pelan kenapa dia begini kepadaku, tetapi sbelumnya aku sudah tau dan aku hanya berpura-pura mendengarkan. &ldquo;nanda, maafkan aku. Aku punya alasan kenapa kita seperti ini. Aku memfitnah kamu mengkhianatiku hingga kita tidak seperti dulu lagi yang appaun kita lakukan bersama. aku bersama arif melakukan ini karena aku tak tahu dengan siapa lagi. Kita sangat berlebihan nan, kita sama-sama tidak berteman dengan teman lainnya hanya karena kita sejak kecil bersama, aku bosan nana.. aku juga ingin bertemu, bermain dengan teman lainnya, bukan hanya denganmu saja. Kita terlalu mebatasi diri hingga pergaulan kita hanya begini saja dan terus menerus. Bahkan kita sudah seperti sepasang kekasih. Aku hanya ingin bebas tanpa merasa menyakitimu ketika aku dengan teman lain...&rdquo;. aku tidak menjawab sepatah dua kata pun kepada nisa, aku pergi begitu saja. Aapakah aku berlebihan? .</p><p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;***</p><p>Beberapa minggu kemudian, aku tersadar bahwa teman bukan membatasi tetapi membebaskan apa yang dia inginkan. Dan aku disini hanya bisa mendukung dan saling menyemangati. Yang aku lakukan dulu ternyata salah, tidak terasa aku memaksa nisa untuk masuk kedalam duniaku tanpa memberi kesempatan kepada nisa untuk menikmati dunianya sendiri. Aku meminta maaf kepada nisa apa yang telah aku lakukan. &ldquo;maafkan aku nisa, dulu aku sangat berlebihan..&rdquo;.&nbsp;</p>', 1, '2020-03-05', 0, 'P'),
(4, 'Sarjana gagal jadi kantoran.................... ', '<p>Sekarang hidup memang benar-benar keras, dulu sejak masih duduk di bangku SMA aku ingin cepat-cepat lulus dan segera ke perguruan tinggi aku pikir di perguruan tinggi itu enak, bebas&nbsp; dan tidak membosankan tetapi berbeda dengan bayanganku.&nbsp; Aku mahasiswa semester akhir di perguruan tinggi swasta di daerah jawa timur. Setelah mengenal dunia perkuliahan, ya awal-awalnya memang menyenangkan, bertemu kawan baru, panggilan guru sudah menjadi bapak/ibu dosen, gedung yang besar dan tingggi serta mewah. Ah sepertinya akan sangat menyenangkan.</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Di pertengahan semester ganjil tepat semester 7, pergi kuliah sudah membosankan, sudah memikirkan banyak hal tentang skripsi, pkl, prposal-proposal dan bahkan pekerjaan. Entah jadi apa aku kelak setelah wisuda dan pergi dari perguruan tinggi ini. Apakah aku hanya mengandalkan gelarku saja yang nanti aku akan mendapat gelar Sarjana 1 Pendidikan SD, bukankah sekarang sarjana satu sudah tidak laku untuk meencari pekerjaan, apalagi dengan sarjana pendidikan SD? Sekolah Dasar? Apakah gelar tersebut hanya bisa untuk menjadi guru sd saja? Bagaimana jika aku mencoba melamar sebagai pegawai dinas pendidikan? Dan&nbsp; atau aku akan menjadi pengangguran?. Sudah menjadi momok bagi ku semester akhir ini untuk berfikir lebih real dan tidak hanya berkhayal.</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Aku belajar dan semakin giat belajar agar nanti ketika wisuda mendapat nilai kumlot dari yang lainya, setiap ujian aku selalu berdoa dan menyempatkan puasa senin-kamis, intinya berusaha. Hingga pada saatnya, aku wisuda dengan nilai terbaik dan resmi mendapat gelar S.Pd.sd. aku pikir dengan memiliki gelar dan nilai terbaik menggampangkanku untuk mencari pekerjaan. Hmmm.... nyatanya susah dan sangat susah, kesana kemari membawa map berisi lamaran pekerjaan semua di tolak dengan alasan kurang memenuhi syarat. Syarat apalagi yang dibutuhkan para pe-rekrut pekerjaan ini? Padahal waktu itu aku sudah menyesuaikan dengan syarat yang berlaku. &nbsp;Daya saing yang begitu kuat hingga aku benar-benar kalah dengan keadaan. Merasa sedih dan lelah dan akhirnya menyerah.</p><p>Beberapa bulan setelah itu, aku bekerja di pasar sebagai penjual bubur kacang hijau dari pagi hingga malam. Siapa bilang itu usaha milikku sendiri? Modal darimana jika itu usahaku? Aku bekerja ikut orang untuk menjaga kios bubur kacang hijaunya. Dan suatu hari sang pemilik bubur tersebut mengajakku bercengkerama, bapak min namanya. Dia menertawaiku dengan alasan kok bisa-bisanya S.Pd.sd berakhirnya jualan bubur kacang hijau dengan gaji yang pas-pas an, aku hanya tersenyum sembari melayani pembeli. Pak min lalu terdiam dan bercerita tntang pengalaman hidupnya, ternyata dia juga sarjana hukum di perguruan tinggi swasta di jawa timur. Aku kaget dan bertanya kenapa bapak tidak menjadi hakim atau bahkan pengacara saja, dia menjawab dengan kepolosannya dan kejujurannya, &ldquo;Dulu memang cita-cita saya adalah pengacara, saya juga melamar sana-sini ditolak karena sudah banyak yang melamar bahkan semua bergelar magister, sempat diterima sebgai hakim dan bekerja tapi hanya bertahan satu tahun &nbsp;karena tiba-tiba ada pergantian posisi dan saya tidak menyetujui. Namun dengan rasa semangat dan dulu pernah ikut pelatihan kerja akhirnya saya membuka kios ini saja dan berjualan bubur kacang hijau. Modal yang saya dapatkan itu dari ibu saya sebelum wafat, beliau telah mempersiapkan untuk hidup saya nantinya&rdquo;.</p><p>Bagaimana? Sungguh indah bukan cerita pak min, dia juga tidak begitu bangga dengan gelarnya, dia hanya bersyukur bisa menyelesaikan S1 nya walaupan tidak bekeja di kantoran. Dan dia bersyukur bisa berwirausaha sendiri dengan tekat dan semangat tanpa rasa malu. Memang sekarang adalah waktu dimana jangan pernah banggakan sarjanamu kalau nanti pada akhirnya kamu lontang-lantung dirumah, menjadi beban orang tua mu dan mempermalukan orang tua juga dirimu sendiri.</p><p>Dengan cerita pak min, aku semakin mencintai pekerjaanku walau hanya penjual bubur kacang hijau, dengan menabung sedikit dari gaji aku akan mengembangkan usaha pak min ini dirumahku sendiri. Aku tidak malu jika kelak ada yang menyepelakanku bahkan mengejeku, karena pada hakikatnya, lebih indah bekerja di kantor sendiri ketimbang ikut orang lain.</p>', 1, '2020-03-05', 7, 'P');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indeks untuk tabel `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_komentar`),
  ADD KEY `id_story` (`id_story`);

--
-- Indeks untuk tabel `story`
--
ALTER TABLE `story`
  ADD PRIMARY KEY (`id_story`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_komentar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `story`
--
ALTER TABLE `story`
  MODIFY `id_story` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `komentar`
--
ALTER TABLE `komentar`
  ADD CONSTRAINT `komentar_ibfk_1` FOREIGN KEY (`id_story`) REFERENCES `story` (`id_story`);

--
-- Ketidakleluasaan untuk tabel `story`
--
ALTER TABLE `story`
  ADD CONSTRAINT `story_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
