<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
          <img src="assets/img/brand/blue.png" class="navbar-brand-img" alt="...">
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="./">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dasbor</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="?page=story">
                <i class="ni ni-collection text-orange"></i>
                <span class="nav-link-text">Story</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="?page=category">
                <i class="ni ni-tag text-primary"></i>
                <span class="nav-link-text">Kategori</span>
              </a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="?page=comment">
                <i class="ni ni-chat-round text-red"></i>
                <span class="nav-link-text">Komentar</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>