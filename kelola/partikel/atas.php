<nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Search form -->
          <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
            <div class="form-group mb-0">
              <div class="input-group input-group-alternative input-group-merge">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-search"></i></span>
                </div>
                <input class="form-control" placeholder="Apa yang Anda cari?" type="text">
              </div>
            </div>
            <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </form>
          <!-- Navbar links -->
          <ul class="navbar-nav align-items-center  ml-md-auto ">
            <li class="nav-item d-xl-none">
              <!-- Sidenav toggler -->
              <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </div>
            </li>
            <li class="nav-item d-sm-none">
              <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                <i class="ni ni-zoom-split-in"></i>
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-chat-round"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
                <!-- Dropdown header -->
                <div class="px-3 py-3">
				<?php
				$sdq = mysqli_query($connection, "SELECT * FROM komentar WHERE baca='N'");
				$htga = mysqli_num_rows($sdq);
				if($htga > 0):
				?>
                  <h6 class="text-sm text-muted m-0">Kamu punya <strong class="text-primary"><?php echo $htga; ?></strong> notifikasi.</h6>
				 <?php
				 else:
				 ?>
				 <h6 class="text-sm text-muted m-0">Sepertinya kosong :(</h6>
				 <?php
				 endif;
				 ?>
                </div>
                <!-- List group -->
                <div class="list-group list-group-flush">
				<?php
				$sd = mysqli_query($connection, "SELECT * FROM komentar WHERE baca='N'");
				$htg = mysqli_num_rows($sd);
				if($htg > 0):
				while ($sdd = mysqli_fetch_array($sd)):
				
				?>
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <!-- Avatar -->
                        <img alt="Image placeholder" src="assets/img/theme/team-1.jpg" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm"><?php echo $sdd['nama']; ?></h4>
                          </div>
                          <div class="text-right text-muted">
                            <small><?php echo $sdd['tgl_komen']; ?></small>
                          </div>
                        </div>
                        <p class="text-sm mb-0"><?php echo $sdd['komentarnya']; ?></p>
                      </div>
                    </div>
                  </a>
                  <?php
				  endwhile;
					else:
					?>
					<a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <!-- Avatar -->
                        
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm"></h4>
                          </div>
                          <div class="text-right text-muted">
                            <small></small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">Tidak ditemukan komentar baru.</p>
                      </div>
                    </div>
                  </a>
					<?php
					endif;
				   ?>
                </div>
                <!-- View all -->
                <a href="#!" class="dropdown-item text-center text-primary font-weight-bold py-3">Lihat Semua</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-eye"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
                <!-- Dropdown header -->
                <div class="px-3 py-3">
				<?php
				$sdaa = mysqli_query($connection, "SELECT SUM(dibaca) as woco FROM story");
				$dt = mysqli_fetch_array($sdaa);
				?>
                  <h6 class="text-sm text-muted m-0">Selamat! <strong class="text-primary"><?php echo $dt['woco']; ?> orang</strong> membaca ceritamu!</h6>
                </div>
                <!-- List group -->
                <div class="list-group list-group-flush">
				<?php
				$sda = mysqli_query($connection, "SELECT * FROM story WHERE dibaca > 5 ORDER BY id_story DESC LIMIT 3");
				$hit = mysqli_num_rows($sda);
				if($hit):
				while($dt = mysqli_fetch_array($sda)):
				?>
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <!-- Avatar -->
                        <img alt="Image placeholder" src="assets/img/theme/team-1.jpg" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">Selamat! Ceritamu sudah dibaca lebih dari 5 orang!</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small></small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">Dalam cerpen <?php echo $dt['judul_story']; ?></p>
                      </div>
                    </div>
                  </a>
                  <?php
				  endwhile;
					else:
					?>
					<a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <!-- Avatar -->
                        
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm"></h4>
                          </div>
                          <div class="text-right text-muted">
                            <small></small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">Ceritamu masih belum laku :(</p>
                      </div>
                    </div>
                  </a>
					<?php
					endif;
				   ?>
                </div>
                <!-- View all -->
                <a href="#!" class="dropdown-item text-center text-primary font-weight-bold py-3">Lihat Semua</a>
              </div>
            </li>
          </ul>
          <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
            <li class="nav-item dropdown">
              <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="../images/thumbnaila2.jpg">
                  </span>
                  <div class="media-body  ml-2  d-none d-lg-block">
                    <span class="mb-0 text-sm  font-weight-bold"><?php echo $_SESSION['nama']; ?></span>
                  </div>
                </div>
              </a>
              <div class="dropdown-menu  dropdown-menu-right ">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0">Welcome!</h6>
                </div>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-single-02"></i>
                  <span>My profile</span>
                </a>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-settings-gear-65"></i>
                  <span>Settings</span>
                </a>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-calendar-grid-58"></i>
                  <span>Activity</span>
                </a>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-support-16"></i>
                  <span>Support</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-user-run"></i>
                  <span>Logout</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>