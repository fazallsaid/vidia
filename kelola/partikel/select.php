<?php
if (isset($_GET['page'])) {
    $page = $_GET['page'];

    switch ($page) {
        
        //pelanggan
        case ('login'):
            include('pages/login/masuk.php');
            break;
		case ('detailpost'):
            include('detail.php');
            break;
        case ('hasil'):
                include('partikel/hasil_cari.php');
                break;
        case ('proses_komentar'):
            include('partikel/proses.php');
            break;
        case ('readprocess'):
            include('pages/komentar/proses.php');
            break;
        case ('story'):
            include('pages/story/story.php');
            break;
        case ('addstory'):
            include('pages/story/tambah.php');
            break;
		case ('editstory'):
            include('pages/story/edit.php');
            break;
        case ('addstoryprocessing'):
            include('pages/story/proses.php');
            break;
		case ('editstoryprocessing'):
            include('pages/story/prosesedit.php');
            break;
		case ('publishstory'):
            include('pages/story/gbpublish.php');
            break;
		case ('draftstory'):
            include('pages/story/gbdraft.php');
            break;
        default:
            include('404.php');
            $title = "404 Not Found";
    }
}else{
    include 'depan.php';
    $title = "Admin Cerpen";
}
?>