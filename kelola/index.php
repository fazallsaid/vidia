<?php
session_start();
//cek apakah user sudah login
if (!isset($_SESSION['uname'])) {
  ?>
        <script language="JavaScript">
            alert('Anda Belum Login. Silahkan klik OK untuk masuk.');
            window.location='pages/login/';
        </script>
<?php
}else{
  $notif = "Kamu berhasil masuk. Selamat bekerja, dan jangan lupa untuk selalu berdoa!";
}
include '../config.php';
include 'partikel/tgl.php';
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Admin Cerpen</title>
  <!-- Favicon -->
  <link rel="icon" href="assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Page plugins -->
  <!-- Argon CSS -->
  <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" type="text/css" href="dist/src/sweetalert.css">
  <script type="text/javascript" src="dist/docs/assets/sweetalert/sweetalert.min.js"></script>
</head>

<body>
  <!-- Sidenav -->
  <?php include 'partikel/menu.php'; ?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include 'partikel/atas.php'; ?>
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Default</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <a href="./">Kembali ke dasbor</a>
                </ol>
              </nav>
            </div>
          </div>
          <!-- Card stats -->
          
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <?php include 'partikel/select.php'; ?>
      <!-- Footer -->
      <?php include 'partikel/kaki.php'; ?>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Optional JS -->
  <script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
  <script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Argon JS -->
  <script src="assets/js/argon.js?v=1.2.0"></script>
    <script src="//cdn.ckeditor.com/4.13.1/basic/ckeditor.js"></script>
  <script>
  CKEDITOR.replace( 'isicerpen' );
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
		var statuss = document.getElementById('statuss');
		$('#statuss').on('change', function() {
			var valueStatue = statuss.options[statuss.selectedIndex].value;
			
			if(valueStatue=='P'){
				$('#tampil-button .help-block').text('Publish Story');
				$('#tampil-button .help-block').css('display', 'block');
			}else if (valueStatue=='D'){
				$('#tampil-button .help-block').text('Draft Story');
				$('#tampil-button .help-block').css('display', 'block');
			}else if (valueStatue==''){
				$('#tampil-button .help-block').text('Draft/Publish Story');
				$('#tampil-button .help-block').css('display', 'block');
			}
		}
	}
</script>
</body>

</html>
