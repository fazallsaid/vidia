<div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Nama</th>
                    <th scope="col">Komentar</th>
                    <th scope="col">Judul Story</th>
					<th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">
				<?php
					$sql = mysqli_query($connection,"SELECT komentar.*, story.* FROM komentar join story on story.id_story=komentar.id_story ORDER BY id_komentar DESC limit 3");
					 
					while($row = mysqli_fetch_array($sql)){
						if ($row['baca']=='N'){
							$tanda = "<i class='bg-primary'></i>";
						}else{
							$tanda = "";
						}
					?>
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        <div class="media-body">
                          <span class="name mb-0 text-sm"><?php echo $row['nama']; ?></span>
                        </div>
                      </div>
                    </th>
                    <td class="budget">
                      <?php echo $row['komentarnya']; ?>
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                        <span class="status"><?php echo $tanda; ?><?php echo $row['judul_story']; ?></span>
                      </span>
                    </td>
					<td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <?php
                        if ($row['baca']=='N'){
                          $update = "Y";
                          $text = "Tandai telah dibaca";
                        }else{
                          $update = "N";
                          $text = "Tandai belum dibaca";
                        }
                        ?>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <form action="?page=readprocess" method="POST">
                          <input type="hidden" name="id_komentar" value="<?php echo $row['id_komentar']; ?>" />
                          <input type="hidden" name="id_story" value="<?php echo $row['id_story']; ?>" />
                          <input type="hidden" name="nama" value="<?php echo $row['nama']; ?>" />
                          <input type="hidden" name="komentarnya" value="<?php echo $row['komentarnya']; ?>" />
                          <input type="hidden" name="tgl_komen" value="<?php echo $row['tgl_komen']; ?>" />
                          <input type="hidden" name="baca" value="<?php echo $update; ?>" />
                          <button class="dropdown-item" type="submit"><?php echo $text; ?></button>  
                        </form>
                          <button class="dropdown-item" type="submit">Hapus</button>
                        </div>
                      </div>
                    </td>
                  </tr>
				  <?php
			}
			?>
                </tbody>
              </table>
              <a href="" class="btn btn-primary">Lihat Semua</a>
            </div>