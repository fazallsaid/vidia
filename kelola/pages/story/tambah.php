<div class="row justify-content-center" id="load_content">
        <div class=" col ">
          <div class="card">
            <div class="card-header bg-transparent">
              <h3 class="mb-0">Tambah cerpenmu</h3>
            </div>
            <div class="card-body">
            <form action="?page=addstoryprocessing" method="POST">
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Judul Cerita</label>
                        <input type="text" name="judul_story" class="form-control" placeholder="Masukkan Judul">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
						<label class="form-control-label">Isi Cerita</label>
						<textarea name="isi_story" rows="8" class="form-control" placeholder="Menulislah ..." id="isicerpen" ></textarea>
					  </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-first-name">Kategori</label>
                        <select name="id_kategori" class="form-control">
						<option value="0">Pilih Kategori</option>
						<?php
						$sel = mysqli_query($connection, "SELECT * FROM kategori");
						while ($sele = mysqli_fetch_array($sel)):
						?>
						<option value="<?php echo $sele['id_kategori']; ?>"><?php echo $sele['nama_kategori']; ?></option>
						<?php
						endwhile;
						?>
						</select>
                      </div>
                    </div>
					<input type="hidden" name="tgl_post" value="<?php echo date('Y-m-d'); ?>" />
					<input type="hidden" name="dibaca" />
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-last-name">Status Cerita</label>
                        <select id="statuss" name="status" class="form-control">
						<option value="">== Pilih Draft atau Publish ==</option>
						<option value="P">Publish</option>
						<option value="D">Draft</option>
						</select>
                      </div>
                    </div>
					<div class="col-lg-6">
                      <div class="form-group" id="tampil-button">
                        <button class="btn btn-success" type="submit">Add Story</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>