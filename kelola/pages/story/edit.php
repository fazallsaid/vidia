<?php
$cerita = $_GET['id_story'];
$az = mysqli_query($connection, "SELECT story.*, kategori.* FROM story JOIN kategori ON kategori.id_kategori=story.id_kategori WHERE id_story='$cerita'");
$azu = mysqli_fetch_array($az);
?>
<div class="row justify-content-center" id="load_content">
        <div class=" col ">
          <div class="card">
            <div class="card-header bg-transparent">
              <h3 class="mb-0">Edit cerpenmu</h3>
            </div>
            <div class="card-body">
            <form action="?page=editstoryprocessing" method="POST">
			<input type="hidden" name="id_story" value="<?php echo $azu['id_story']; ?>" />
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Judul Cerita</label>
                        <input type="text" name="judul_story" class="form-control" value="<?php echo $azu['judul_story']; ?>">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
						<label class="form-control-label">Isi Cerita</label>
						<textarea name="isi_story" rows="8" class="form-control" id="isicerpen" ><?php echo $azu['isi_story']; ?></textarea>
					  </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-first-name">Kategori</label>
                        <select name="id_kategori" class="form-control">
						<option value="<?php echo $azu['id_kategori']; ?>"><?php echo $azu['nama_kategori']; ?> (Dipilih)</option>
						<?php
						$sel = mysqli_query($connection, "SELECT * FROM kategori WHERE id_kategori NOT IN ($azu[id_kategori])");
						while ($sele = mysqli_fetch_array($sel)):
						?>
						<option value="<?php echo $sele['id_kategori']; ?>"><?php echo $sele['nama_kategori']; ?></option>
						<?php
						endwhile;
						?>
						</select>
                      </div>
                    </div>
					<input type="hidden" name="tgl_post" value="<?php echo $azu['tgl_post']; ?>" />
					<input type="hidden" name="dibaca" value="<?php echo $azu['dibaca']; ?>" />
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-last-name">Status Cerita</label>
                        <select id="statuss" name="status" class="form-control">
						<option value="<?php echo $azu['status']; ?>"><?php echo $azu['status']; ?> (Dipilih)</option>
						<option value="P">Publish</option>
						<option value="D">Draft</option>
						</select>
                      </div>
                    </div>
					<div class="col-lg-6">
                      <div class="form-group" id="tampil-button">
                        <button class="btn btn-success" type="submit">Edit Story</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>